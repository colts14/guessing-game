﻿using System.Collections.Generic;

namespace Assignment06_Group06
{
    //
    // Extension that downcasts the object in dictionary and returns it to the calling method
    //
    static class DictionaryExtension
    {
        public static T Get<T>(this Dictionary<string, object> dictionaryInstance, string dictionaryKey)
        {
            return (T)dictionaryInstance[dictionaryKey];
        }
    }
}
