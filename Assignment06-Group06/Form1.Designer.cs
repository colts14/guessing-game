﻿namespace Assignment06_Group06
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button97 = new System.Windows.Forms.Button();
            this.button98 = new System.Windows.Forms.Button();
            this.button99 = new System.Windows.Forms.Button();
            this.button100 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.button93 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.button95 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.button91 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button81 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtShowMessage = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblShowScore = new System.Windows.Forms.Label();
            this.lblLives = new System.Windows.Forms.Label();
            this.lblShowLives = new System.Windows.Forms.Label();
            this.lblPlayAgainMessage = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button97);
            this.groupBox1.Controls.Add(this.button98);
            this.groupBox1.Controls.Add(this.button99);
            this.groupBox1.Controls.Add(this.button100);
            this.groupBox1.Controls.Add(this.button92);
            this.groupBox1.Controls.Add(this.button93);
            this.groupBox1.Controls.Add(this.button94);
            this.groupBox1.Controls.Add(this.button95);
            this.groupBox1.Controls.Add(this.button96);
            this.groupBox1.Controls.Add(this.button91);
            this.groupBox1.Controls.Add(this.button87);
            this.groupBox1.Controls.Add(this.button88);
            this.groupBox1.Controls.Add(this.button89);
            this.groupBox1.Controls.Add(this.button90);
            this.groupBox1.Controls.Add(this.button82);
            this.groupBox1.Controls.Add(this.button83);
            this.groupBox1.Controls.Add(this.button84);
            this.groupBox1.Controls.Add(this.button85);
            this.groupBox1.Controls.Add(this.button86);
            this.groupBox1.Controls.Add(this.button81);
            this.groupBox1.Controls.Add(this.button77);
            this.groupBox1.Controls.Add(this.button78);
            this.groupBox1.Controls.Add(this.button79);
            this.groupBox1.Controls.Add(this.button80);
            this.groupBox1.Controls.Add(this.button72);
            this.groupBox1.Controls.Add(this.button73);
            this.groupBox1.Controls.Add(this.button74);
            this.groupBox1.Controls.Add(this.button75);
            this.groupBox1.Controls.Add(this.button76);
            this.groupBox1.Controls.Add(this.button71);
            this.groupBox1.Controls.Add(this.button67);
            this.groupBox1.Controls.Add(this.button68);
            this.groupBox1.Controls.Add(this.button69);
            this.groupBox1.Controls.Add(this.button70);
            this.groupBox1.Controls.Add(this.button62);
            this.groupBox1.Controls.Add(this.button63);
            this.groupBox1.Controls.Add(this.button64);
            this.groupBox1.Controls.Add(this.button65);
            this.groupBox1.Controls.Add(this.button66);
            this.groupBox1.Controls.Add(this.button61);
            this.groupBox1.Controls.Add(this.button57);
            this.groupBox1.Controls.Add(this.button58);
            this.groupBox1.Controls.Add(this.button59);
            this.groupBox1.Controls.Add(this.button60);
            this.groupBox1.Controls.Add(this.button52);
            this.groupBox1.Controls.Add(this.button53);
            this.groupBox1.Controls.Add(this.button54);
            this.groupBox1.Controls.Add(this.button55);
            this.groupBox1.Controls.Add(this.button56);
            this.groupBox1.Controls.Add(this.button51);
            this.groupBox1.Controls.Add(this.button47);
            this.groupBox1.Controls.Add(this.button48);
            this.groupBox1.Controls.Add(this.button49);
            this.groupBox1.Controls.Add(this.button50);
            this.groupBox1.Controls.Add(this.button42);
            this.groupBox1.Controls.Add(this.button43);
            this.groupBox1.Controls.Add(this.button44);
            this.groupBox1.Controls.Add(this.button45);
            this.groupBox1.Controls.Add(this.button46);
            this.groupBox1.Controls.Add(this.button41);
            this.groupBox1.Controls.Add(this.button37);
            this.groupBox1.Controls.Add(this.button38);
            this.groupBox1.Controls.Add(this.button39);
            this.groupBox1.Controls.Add(this.button40);
            this.groupBox1.Controls.Add(this.button32);
            this.groupBox1.Controls.Add(this.button33);
            this.groupBox1.Controls.Add(this.button34);
            this.groupBox1.Controls.Add(this.button35);
            this.groupBox1.Controls.Add(this.button36);
            this.groupBox1.Controls.Add(this.button31);
            this.groupBox1.Controls.Add(this.button27);
            this.groupBox1.Controls.Add(this.button28);
            this.groupBox1.Controls.Add(this.button29);
            this.groupBox1.Controls.Add(this.button30);
            this.groupBox1.Controls.Add(this.button22);
            this.groupBox1.Controls.Add(this.button23);
            this.groupBox1.Controls.Add(this.button24);
            this.groupBox1.Controls.Add(this.button25);
            this.groupBox1.Controls.Add(this.button26);
            this.groupBox1.Controls.Add(this.button21);
            this.groupBox1.Controls.Add(this.button17);
            this.groupBox1.Controls.Add(this.button18);
            this.groupBox1.Controls.Add(this.button19);
            this.groupBox1.Controls.Add(this.button20);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.button14);
            this.groupBox1.Controls.Add(this.button15);
            this.groupBox1.Controls.Add(this.button16);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(429, 412);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Guess The Number";
            // 
            // button97
            // 
            this.button97.BackColor = System.Drawing.Color.White;
            this.button97.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button97.FlatAppearance.BorderSize = 0;
            this.button97.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button97.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button97.Location = new System.Drawing.Point(253, 358);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(40, 38);
            this.button97.TabIndex = 99;
            this.button97.Text = "97";
            this.button97.UseVisualStyleBackColor = false;
            // 
            // button98
            // 
            this.button98.BackColor = System.Drawing.Color.White;
            this.button98.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button98.FlatAppearance.BorderSize = 0;
            this.button98.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button98.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button98.Location = new System.Drawing.Point(292, 358);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(40, 38);
            this.button98.TabIndex = 98;
            this.button98.Text = "98";
            this.button98.UseVisualStyleBackColor = false;
            // 
            // button99
            // 
            this.button99.BackColor = System.Drawing.Color.White;
            this.button99.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button99.FlatAppearance.BorderSize = 0;
            this.button99.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button99.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button99.Location = new System.Drawing.Point(331, 358);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(40, 38);
            this.button99.TabIndex = 97;
            this.button99.Text = "99";
            this.button99.UseVisualStyleBackColor = false;
            // 
            // button100
            // 
            this.button100.BackColor = System.Drawing.Color.White;
            this.button100.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button100.FlatAppearance.BorderSize = 0;
            this.button100.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button100.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button100.Location = new System.Drawing.Point(370, 358);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(40, 38);
            this.button100.TabIndex = 96;
            this.button100.Text = "100";
            this.button100.UseVisualStyleBackColor = false;
            // 
            // button92
            // 
            this.button92.BackColor = System.Drawing.Color.White;
            this.button92.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button92.FlatAppearance.BorderSize = 0;
            this.button92.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button92.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button92.Location = new System.Drawing.Point(58, 358);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(40, 38);
            this.button92.TabIndex = 95;
            this.button92.Text = "92";
            this.button92.UseVisualStyleBackColor = false;
            // 
            // button93
            // 
            this.button93.BackColor = System.Drawing.Color.White;
            this.button93.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button93.FlatAppearance.BorderSize = 0;
            this.button93.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button93.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button93.Location = new System.Drawing.Point(97, 358);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(40, 38);
            this.button93.TabIndex = 94;
            this.button93.Text = "93";
            this.button93.UseVisualStyleBackColor = false;
            // 
            // button94
            // 
            this.button94.BackColor = System.Drawing.Color.White;
            this.button94.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button94.FlatAppearance.BorderSize = 0;
            this.button94.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button94.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button94.Location = new System.Drawing.Point(136, 358);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(40, 38);
            this.button94.TabIndex = 93;
            this.button94.Text = "94";
            this.button94.UseVisualStyleBackColor = false;
            // 
            // button95
            // 
            this.button95.BackColor = System.Drawing.Color.White;
            this.button95.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button95.FlatAppearance.BorderSize = 0;
            this.button95.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button95.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button95.Location = new System.Drawing.Point(175, 358);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(40, 38);
            this.button95.TabIndex = 92;
            this.button95.Text = "95";
            this.button95.UseVisualStyleBackColor = false;
            // 
            // button96
            // 
            this.button96.BackColor = System.Drawing.Color.White;
            this.button96.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button96.FlatAppearance.BorderSize = 0;
            this.button96.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button96.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button96.Location = new System.Drawing.Point(214, 358);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(40, 38);
            this.button96.TabIndex = 91;
            this.button96.Text = "96";
            this.button96.UseVisualStyleBackColor = false;
            // 
            // button91
            // 
            this.button91.BackColor = System.Drawing.Color.White;
            this.button91.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button91.FlatAppearance.BorderSize = 0;
            this.button91.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button91.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button91.Location = new System.Drawing.Point(19, 358);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(40, 38);
            this.button91.TabIndex = 90;
            this.button91.Text = "91";
            this.button91.UseVisualStyleBackColor = false;
            // 
            // button87
            // 
            this.button87.BackColor = System.Drawing.Color.White;
            this.button87.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button87.FlatAppearance.BorderSize = 0;
            this.button87.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button87.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button87.Location = new System.Drawing.Point(253, 321);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(40, 38);
            this.button87.TabIndex = 89;
            this.button87.Text = "87";
            this.button87.UseVisualStyleBackColor = false;
            // 
            // button88
            // 
            this.button88.BackColor = System.Drawing.Color.White;
            this.button88.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button88.FlatAppearance.BorderSize = 0;
            this.button88.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button88.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button88.Location = new System.Drawing.Point(292, 321);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(40, 38);
            this.button88.TabIndex = 88;
            this.button88.Text = "88";
            this.button88.UseVisualStyleBackColor = false;
            // 
            // button89
            // 
            this.button89.BackColor = System.Drawing.Color.White;
            this.button89.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button89.FlatAppearance.BorderSize = 0;
            this.button89.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button89.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button89.Location = new System.Drawing.Point(331, 321);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(40, 38);
            this.button89.TabIndex = 87;
            this.button89.Text = "89";
            this.button89.UseVisualStyleBackColor = false;
            // 
            // button90
            // 
            this.button90.BackColor = System.Drawing.Color.White;
            this.button90.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button90.FlatAppearance.BorderSize = 0;
            this.button90.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button90.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button90.Location = new System.Drawing.Point(370, 321);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(40, 38);
            this.button90.TabIndex = 86;
            this.button90.Text = "90";
            this.button90.UseVisualStyleBackColor = false;
            // 
            // button82
            // 
            this.button82.BackColor = System.Drawing.Color.White;
            this.button82.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button82.FlatAppearance.BorderSize = 0;
            this.button82.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button82.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button82.Location = new System.Drawing.Point(58, 321);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(40, 38);
            this.button82.TabIndex = 85;
            this.button82.Text = "82";
            this.button82.UseVisualStyleBackColor = false;
            // 
            // button83
            // 
            this.button83.BackColor = System.Drawing.Color.White;
            this.button83.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button83.FlatAppearance.BorderSize = 0;
            this.button83.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button83.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button83.Location = new System.Drawing.Point(97, 321);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(40, 38);
            this.button83.TabIndex = 84;
            this.button83.Text = "83";
            this.button83.UseVisualStyleBackColor = false;
            // 
            // button84
            // 
            this.button84.BackColor = System.Drawing.Color.White;
            this.button84.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button84.FlatAppearance.BorderSize = 0;
            this.button84.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button84.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button84.Location = new System.Drawing.Point(136, 321);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(40, 38);
            this.button84.TabIndex = 83;
            this.button84.Text = "84";
            this.button84.UseVisualStyleBackColor = false;
            // 
            // button85
            // 
            this.button85.BackColor = System.Drawing.Color.White;
            this.button85.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button85.FlatAppearance.BorderSize = 0;
            this.button85.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button85.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button85.Location = new System.Drawing.Point(175, 321);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(40, 38);
            this.button85.TabIndex = 82;
            this.button85.Text = "85";
            this.button85.UseVisualStyleBackColor = false;
            // 
            // button86
            // 
            this.button86.BackColor = System.Drawing.Color.White;
            this.button86.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button86.FlatAppearance.BorderSize = 0;
            this.button86.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button86.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button86.Location = new System.Drawing.Point(214, 321);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(40, 38);
            this.button86.TabIndex = 81;
            this.button86.Text = "86";
            this.button86.UseVisualStyleBackColor = false;
            // 
            // button81
            // 
            this.button81.BackColor = System.Drawing.Color.White;
            this.button81.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button81.FlatAppearance.BorderSize = 0;
            this.button81.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button81.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button81.Location = new System.Drawing.Point(19, 321);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(40, 38);
            this.button81.TabIndex = 80;
            this.button81.Text = "81";
            this.button81.UseVisualStyleBackColor = false;
            // 
            // button77
            // 
            this.button77.BackColor = System.Drawing.Color.White;
            this.button77.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button77.FlatAppearance.BorderSize = 0;
            this.button77.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button77.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button77.Location = new System.Drawing.Point(253, 284);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(40, 38);
            this.button77.TabIndex = 79;
            this.button77.Text = "77";
            this.button77.UseVisualStyleBackColor = false;
            // 
            // button78
            // 
            this.button78.BackColor = System.Drawing.Color.White;
            this.button78.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button78.FlatAppearance.BorderSize = 0;
            this.button78.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button78.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button78.Location = new System.Drawing.Point(292, 284);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(40, 38);
            this.button78.TabIndex = 78;
            this.button78.Text = "78";
            this.button78.UseVisualStyleBackColor = false;
            // 
            // button79
            // 
            this.button79.BackColor = System.Drawing.Color.White;
            this.button79.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button79.FlatAppearance.BorderSize = 0;
            this.button79.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button79.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button79.Location = new System.Drawing.Point(331, 284);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(40, 38);
            this.button79.TabIndex = 77;
            this.button79.Text = "79";
            this.button79.UseVisualStyleBackColor = false;
            // 
            // button80
            // 
            this.button80.BackColor = System.Drawing.Color.White;
            this.button80.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button80.FlatAppearance.BorderSize = 0;
            this.button80.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button80.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button80.Location = new System.Drawing.Point(370, 284);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(40, 38);
            this.button80.TabIndex = 76;
            this.button80.Text = "80";
            this.button80.UseVisualStyleBackColor = false;
            // 
            // button72
            // 
            this.button72.BackColor = System.Drawing.Color.White;
            this.button72.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button72.FlatAppearance.BorderSize = 0;
            this.button72.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button72.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button72.Location = new System.Drawing.Point(58, 284);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(40, 38);
            this.button72.TabIndex = 75;
            this.button72.Text = "72";
            this.button72.UseVisualStyleBackColor = false;
            // 
            // button73
            // 
            this.button73.BackColor = System.Drawing.Color.White;
            this.button73.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button73.FlatAppearance.BorderSize = 0;
            this.button73.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button73.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button73.Location = new System.Drawing.Point(97, 284);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(40, 38);
            this.button73.TabIndex = 74;
            this.button73.Text = "73";
            this.button73.UseVisualStyleBackColor = false;
            // 
            // button74
            // 
            this.button74.BackColor = System.Drawing.Color.White;
            this.button74.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button74.FlatAppearance.BorderSize = 0;
            this.button74.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button74.Location = new System.Drawing.Point(136, 284);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(40, 38);
            this.button74.TabIndex = 73;
            this.button74.Text = "74";
            this.button74.UseVisualStyleBackColor = false;
            // 
            // button75
            // 
            this.button75.BackColor = System.Drawing.Color.White;
            this.button75.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button75.FlatAppearance.BorderSize = 0;
            this.button75.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button75.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button75.Location = new System.Drawing.Point(175, 284);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(40, 38);
            this.button75.TabIndex = 72;
            this.button75.Text = "75";
            this.button75.UseVisualStyleBackColor = false;
            // 
            // button76
            // 
            this.button76.BackColor = System.Drawing.Color.White;
            this.button76.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button76.FlatAppearance.BorderSize = 0;
            this.button76.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button76.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button76.Location = new System.Drawing.Point(214, 284);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(40, 38);
            this.button76.TabIndex = 71;
            this.button76.Text = "76";
            this.button76.UseVisualStyleBackColor = false;
            // 
            // button71
            // 
            this.button71.BackColor = System.Drawing.Color.White;
            this.button71.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button71.FlatAppearance.BorderSize = 0;
            this.button71.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button71.Location = new System.Drawing.Point(19, 284);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(40, 38);
            this.button71.TabIndex = 70;
            this.button71.Text = "71";
            this.button71.UseVisualStyleBackColor = false;
            // 
            // button67
            // 
            this.button67.BackColor = System.Drawing.Color.White;
            this.button67.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button67.FlatAppearance.BorderSize = 0;
            this.button67.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button67.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button67.Location = new System.Drawing.Point(253, 247);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(40, 38);
            this.button67.TabIndex = 69;
            this.button67.Text = "67";
            this.button67.UseVisualStyleBackColor = false;
            // 
            // button68
            // 
            this.button68.BackColor = System.Drawing.Color.White;
            this.button68.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button68.FlatAppearance.BorderSize = 0;
            this.button68.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button68.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button68.Location = new System.Drawing.Point(292, 247);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(40, 38);
            this.button68.TabIndex = 68;
            this.button68.Text = "68";
            this.button68.UseVisualStyleBackColor = false;
            // 
            // button69
            // 
            this.button69.BackColor = System.Drawing.Color.White;
            this.button69.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button69.FlatAppearance.BorderSize = 0;
            this.button69.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button69.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button69.Location = new System.Drawing.Point(331, 247);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(40, 38);
            this.button69.TabIndex = 67;
            this.button69.Text = "69";
            this.button69.UseVisualStyleBackColor = false;
            // 
            // button70
            // 
            this.button70.BackColor = System.Drawing.Color.White;
            this.button70.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button70.FlatAppearance.BorderSize = 0;
            this.button70.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button70.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button70.Location = new System.Drawing.Point(370, 247);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(40, 38);
            this.button70.TabIndex = 66;
            this.button70.Text = "70";
            this.button70.UseVisualStyleBackColor = false;
            // 
            // button62
            // 
            this.button62.BackColor = System.Drawing.Color.White;
            this.button62.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button62.FlatAppearance.BorderSize = 0;
            this.button62.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button62.Location = new System.Drawing.Point(58, 247);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(40, 38);
            this.button62.TabIndex = 65;
            this.button62.Text = "62";
            this.button62.UseVisualStyleBackColor = false;
            // 
            // button63
            // 
            this.button63.BackColor = System.Drawing.Color.White;
            this.button63.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button63.FlatAppearance.BorderSize = 0;
            this.button63.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button63.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button63.Location = new System.Drawing.Point(97, 247);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(40, 38);
            this.button63.TabIndex = 64;
            this.button63.Text = "63";
            this.button63.UseVisualStyleBackColor = false;
            // 
            // button64
            // 
            this.button64.BackColor = System.Drawing.Color.White;
            this.button64.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button64.FlatAppearance.BorderSize = 0;
            this.button64.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button64.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button64.Location = new System.Drawing.Point(136, 247);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(40, 38);
            this.button64.TabIndex = 63;
            this.button64.Text = "64";
            this.button64.UseVisualStyleBackColor = false;
            // 
            // button65
            // 
            this.button65.BackColor = System.Drawing.Color.White;
            this.button65.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button65.FlatAppearance.BorderSize = 0;
            this.button65.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button65.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button65.Location = new System.Drawing.Point(175, 247);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(40, 38);
            this.button65.TabIndex = 62;
            this.button65.Text = "65";
            this.button65.UseVisualStyleBackColor = false;
            // 
            // button66
            // 
            this.button66.BackColor = System.Drawing.Color.White;
            this.button66.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button66.FlatAppearance.BorderSize = 0;
            this.button66.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button66.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button66.Location = new System.Drawing.Point(214, 247);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(40, 38);
            this.button66.TabIndex = 61;
            this.button66.Text = "66";
            this.button66.UseVisualStyleBackColor = false;
            // 
            // button61
            // 
            this.button61.BackColor = System.Drawing.Color.White;
            this.button61.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button61.FlatAppearance.BorderSize = 0;
            this.button61.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button61.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button61.Location = new System.Drawing.Point(19, 247);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(40, 38);
            this.button61.TabIndex = 60;
            this.button61.Text = "61";
            this.button61.UseVisualStyleBackColor = false;
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.Color.White;
            this.button57.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button57.FlatAppearance.BorderSize = 0;
            this.button57.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button57.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button57.Location = new System.Drawing.Point(253, 210);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(40, 38);
            this.button57.TabIndex = 59;
            this.button57.Text = "57";
            this.button57.UseVisualStyleBackColor = false;
            // 
            // button58
            // 
            this.button58.BackColor = System.Drawing.Color.White;
            this.button58.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button58.FlatAppearance.BorderSize = 0;
            this.button58.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button58.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button58.Location = new System.Drawing.Point(292, 210);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(40, 38);
            this.button58.TabIndex = 58;
            this.button58.Text = "58";
            this.button58.UseVisualStyleBackColor = false;
            // 
            // button59
            // 
            this.button59.BackColor = System.Drawing.Color.White;
            this.button59.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button59.FlatAppearance.BorderSize = 0;
            this.button59.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button59.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button59.Location = new System.Drawing.Point(331, 210);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(40, 38);
            this.button59.TabIndex = 57;
            this.button59.Text = "59";
            this.button59.UseVisualStyleBackColor = false;
            // 
            // button60
            // 
            this.button60.BackColor = System.Drawing.Color.White;
            this.button60.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button60.FlatAppearance.BorderSize = 0;
            this.button60.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button60.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button60.Location = new System.Drawing.Point(370, 210);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(40, 38);
            this.button60.TabIndex = 56;
            this.button60.Text = "60";
            this.button60.UseVisualStyleBackColor = false;
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.Color.White;
            this.button52.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button52.FlatAppearance.BorderSize = 0;
            this.button52.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button52.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button52.Location = new System.Drawing.Point(58, 210);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(40, 38);
            this.button52.TabIndex = 55;
            this.button52.Text = "52";
            this.button52.UseVisualStyleBackColor = false;
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.White;
            this.button53.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button53.FlatAppearance.BorderSize = 0;
            this.button53.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button53.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button53.Location = new System.Drawing.Point(97, 210);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(40, 38);
            this.button53.TabIndex = 54;
            this.button53.Text = "53";
            this.button53.UseVisualStyleBackColor = false;
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.White;
            this.button54.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button54.FlatAppearance.BorderSize = 0;
            this.button54.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button54.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button54.Location = new System.Drawing.Point(136, 210);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(40, 38);
            this.button54.TabIndex = 53;
            this.button54.Text = "54";
            this.button54.UseVisualStyleBackColor = false;
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.White;
            this.button55.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button55.FlatAppearance.BorderSize = 0;
            this.button55.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button55.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button55.Location = new System.Drawing.Point(175, 210);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(40, 38);
            this.button55.TabIndex = 52;
            this.button55.Text = "55";
            this.button55.UseVisualStyleBackColor = false;
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.Color.White;
            this.button56.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button56.FlatAppearance.BorderSize = 0;
            this.button56.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button56.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button56.Location = new System.Drawing.Point(214, 210);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(40, 38);
            this.button56.TabIndex = 51;
            this.button56.Text = "56";
            this.button56.UseVisualStyleBackColor = false;
            // 
            // button51
            // 
            this.button51.BackColor = System.Drawing.Color.White;
            this.button51.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button51.FlatAppearance.BorderSize = 0;
            this.button51.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button51.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button51.Location = new System.Drawing.Point(19, 210);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(40, 38);
            this.button51.TabIndex = 50;
            this.button51.Text = "51";
            this.button51.UseVisualStyleBackColor = false;
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.Color.White;
            this.button47.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button47.FlatAppearance.BorderSize = 0;
            this.button47.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button47.Location = new System.Drawing.Point(253, 173);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(40, 38);
            this.button47.TabIndex = 49;
            this.button47.Text = "47";
            this.button47.UseVisualStyleBackColor = false;
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.Color.White;
            this.button48.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button48.FlatAppearance.BorderSize = 0;
            this.button48.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button48.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button48.Location = new System.Drawing.Point(292, 173);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(40, 38);
            this.button48.TabIndex = 48;
            this.button48.Text = "48";
            this.button48.UseVisualStyleBackColor = false;
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.Color.White;
            this.button49.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button49.FlatAppearance.BorderSize = 0;
            this.button49.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button49.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button49.Location = new System.Drawing.Point(331, 173);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(40, 38);
            this.button49.TabIndex = 47;
            this.button49.Text = "49";
            this.button49.UseVisualStyleBackColor = false;
            // 
            // button50
            // 
            this.button50.BackColor = System.Drawing.Color.White;
            this.button50.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button50.FlatAppearance.BorderSize = 0;
            this.button50.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button50.Location = new System.Drawing.Point(370, 173);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(40, 38);
            this.button50.TabIndex = 46;
            this.button50.Text = "50";
            this.button50.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.White;
            this.button42.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button42.FlatAppearance.BorderSize = 0;
            this.button42.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button42.Location = new System.Drawing.Point(58, 173);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(40, 38);
            this.button42.TabIndex = 45;
            this.button42.Text = "42";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.White;
            this.button43.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button43.FlatAppearance.BorderSize = 0;
            this.button43.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button43.Location = new System.Drawing.Point(97, 173);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(40, 38);
            this.button43.TabIndex = 44;
            this.button43.Text = "43";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.White;
            this.button44.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button44.FlatAppearance.BorderSize = 0;
            this.button44.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button44.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button44.Location = new System.Drawing.Point(136, 173);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(40, 38);
            this.button44.TabIndex = 43;
            this.button44.Text = "44";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.Color.White;
            this.button45.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button45.FlatAppearance.BorderSize = 0;
            this.button45.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button45.Location = new System.Drawing.Point(175, 173);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(40, 38);
            this.button45.TabIndex = 42;
            this.button45.Text = "45";
            this.button45.UseVisualStyleBackColor = false;
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.White;
            this.button46.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button46.FlatAppearance.BorderSize = 0;
            this.button46.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button46.Location = new System.Drawing.Point(214, 173);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(40, 38);
            this.button46.TabIndex = 41;
            this.button46.Text = "46";
            this.button46.UseVisualStyleBackColor = false;
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.Color.White;
            this.button41.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button41.FlatAppearance.BorderSize = 0;
            this.button41.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button41.Location = new System.Drawing.Point(19, 173);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(40, 38);
            this.button41.TabIndex = 40;
            this.button41.Text = "41";
            this.button41.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.White;
            this.button37.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button37.FlatAppearance.BorderSize = 0;
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button37.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.Location = new System.Drawing.Point(253, 136);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(40, 38);
            this.button37.TabIndex = 39;
            this.button37.Text = "37";
            this.button37.UseVisualStyleBackColor = false;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.White;
            this.button38.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button38.FlatAppearance.BorderSize = 0;
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button38.Location = new System.Drawing.Point(292, 136);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(40, 38);
            this.button38.TabIndex = 38;
            this.button38.Text = "38";
            this.button38.UseVisualStyleBackColor = false;
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.White;
            this.button39.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button39.FlatAppearance.BorderSize = 0;
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button39.Location = new System.Drawing.Point(331, 136);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(40, 38);
            this.button39.TabIndex = 37;
            this.button39.Text = "39";
            this.button39.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.White;
            this.button40.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button40.FlatAppearance.BorderSize = 0;
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button40.Location = new System.Drawing.Point(370, 136);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(40, 38);
            this.button40.TabIndex = 36;
            this.button40.Text = "40";
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.White;
            this.button32.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button32.FlatAppearance.BorderSize = 0;
            this.button32.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button32.Location = new System.Drawing.Point(58, 136);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(40, 38);
            this.button32.TabIndex = 35;
            this.button32.Text = "32";
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.White;
            this.button33.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button33.FlatAppearance.BorderSize = 0;
            this.button33.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.Location = new System.Drawing.Point(97, 136);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(40, 38);
            this.button33.TabIndex = 34;
            this.button33.Text = "33";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.White;
            this.button34.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button34.FlatAppearance.BorderSize = 0;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button34.Location = new System.Drawing.Point(136, 136);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(40, 38);
            this.button34.TabIndex = 33;
            this.button34.Text = "34";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.White;
            this.button35.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button35.FlatAppearance.BorderSize = 0;
            this.button35.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button35.Location = new System.Drawing.Point(175, 136);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(40, 38);
            this.button35.TabIndex = 32;
            this.button35.Text = "35";
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.White;
            this.button36.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button36.FlatAppearance.BorderSize = 0;
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button36.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.Location = new System.Drawing.Point(214, 136);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(40, 38);
            this.button36.TabIndex = 31;
            this.button36.Text = "36";
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.White;
            this.button31.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button31.FlatAppearance.BorderSize = 0;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.Location = new System.Drawing.Point(19, 136);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(40, 38);
            this.button31.TabIndex = 30;
            this.button31.Text = "31";
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.White;
            this.button27.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button27.FlatAppearance.BorderSize = 0;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button27.Location = new System.Drawing.Point(253, 99);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(40, 38);
            this.button27.TabIndex = 29;
            this.button27.Text = "27";
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.White;
            this.button28.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button28.Location = new System.Drawing.Point(292, 99);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(40, 38);
            this.button28.TabIndex = 28;
            this.button28.Text = "28";
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.White;
            this.button29.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button29.FlatAppearance.BorderSize = 0;
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.Location = new System.Drawing.Point(331, 99);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(40, 38);
            this.button29.TabIndex = 27;
            this.button29.Text = "29";
            this.button29.UseVisualStyleBackColor = false;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.White;
            this.button30.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button30.FlatAppearance.BorderSize = 0;
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.Location = new System.Drawing.Point(370, 99);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(40, 38);
            this.button30.TabIndex = 26;
            this.button30.Text = "30";
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.White;
            this.button22.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.Location = new System.Drawing.Point(58, 99);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(40, 38);
            this.button22.TabIndex = 25;
            this.button22.Text = "22";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.White;
            this.button23.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button23.FlatAppearance.BorderSize = 0;
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.Location = new System.Drawing.Point(97, 99);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 38);
            this.button23.TabIndex = 24;
            this.button23.Text = "23";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.White;
            this.button24.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button24.FlatAppearance.BorderSize = 0;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(136, 99);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(40, 38);
            this.button24.TabIndex = 23;
            this.button24.Text = "24";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.White;
            this.button25.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button25.FlatAppearance.BorderSize = 0;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.Location = new System.Drawing.Point(175, 99);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(40, 38);
            this.button25.TabIndex = 22;
            this.button25.Text = "25";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.White;
            this.button26.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button26.FlatAppearance.BorderSize = 0;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.Location = new System.Drawing.Point(214, 99);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(40, 38);
            this.button26.TabIndex = 21;
            this.button26.Text = "26";
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.White;
            this.button21.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button21.FlatAppearance.BorderSize = 0;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Location = new System.Drawing.Point(19, 99);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(40, 38);
            this.button21.TabIndex = 20;
            this.button21.Text = "21";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.White;
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(253, 62);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(40, 38);
            this.button17.TabIndex = 19;
            this.button17.Text = "17";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.White;
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(292, 62);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(40, 38);
            this.button18.TabIndex = 18;
            this.button18.Text = "18";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.White;
            this.button19.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button19.FlatAppearance.BorderSize = 0;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Location = new System.Drawing.Point(331, 62);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(40, 38);
            this.button19.TabIndex = 17;
            this.button19.Text = "19";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.White;
            this.button20.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Location = new System.Drawing.Point(370, 62);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(40, 38);
            this.button20.TabIndex = 16;
            this.button20.Text = "20";
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(58, 62);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(40, 38);
            this.button12.TabIndex = 15;
            this.button12.Text = "12";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(97, 62);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(40, 38);
            this.button13.TabIndex = 14;
            this.button13.Text = "13";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.White;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(136, 62);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(40, 38);
            this.button14.TabIndex = 13;
            this.button14.Text = "14";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.White;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(175, 62);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(40, 38);
            this.button15.TabIndex = 12;
            this.button15.Text = "15";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.White;
            this.button16.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Location = new System.Drawing.Point(214, 62);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(40, 38);
            this.button16.TabIndex = 11;
            this.button16.Text = "16";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(19, 62);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(40, 38);
            this.button11.TabIndex = 10;
            this.button11.Text = "11";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(253, 25);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 38);
            this.button7.TabIndex = 9;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(292, 25);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 38);
            this.button8.TabIndex = 8;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.White;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(331, 25);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 38);
            this.button9.TabIndex = 7;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(370, 25);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(40, 38);
            this.button10.TabIndex = 6;
            this.button10.Text = "10";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(58, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 38);
            this.button2.TabIndex = 5;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(97, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 38);
            this.button3.TabIndex = 4;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(136, 25);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 38);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(175, 25);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 38);
            this.button5.TabIndex = 2;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(214, 25);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 38);
            this.button6.TabIndex = 1;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(19, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnStart
            // 
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.btnStart.FlatAppearance.BorderSize = 2;
            this.btnStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.btnStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnStart.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(461, 371);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(126, 38);
            this.btnStart.TabIndex = 1;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblScore
            // 
            this.lblScore.BackColor = System.Drawing.Color.SlateGray;
            this.lblScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblScore.Font = new System.Drawing.Font("Arial", 10F);
            this.lblScore.ForeColor = System.Drawing.Color.White;
            this.lblScore.Location = new System.Drawing.Point(494, 54);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(94, 29);
            this.lblScore.TabIndex = 2;
            this.lblScore.Text = "Score:";
            this.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.lblMessage.Location = new System.Drawing.Point(553, 183);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(69, 16);
            this.lblMessage.TabIndex = 6;
            this.lblMessage.Text = "Message:";
            // 
            // txtShowMessage
            // 
            this.txtShowMessage.BackColor = System.Drawing.SystemColors.Menu;
            this.txtShowMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShowMessage.Location = new System.Drawing.Point(462, 210);
            this.txtShowMessage.Multiline = true;
            this.txtShowMessage.Name = "txtShowMessage";
            this.txtShowMessage.ReadOnly = true;
            this.txtShowMessage.Size = new System.Drawing.Size(259, 77);
            this.txtShowMessage.TabIndex = 7;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnExit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(596, 371);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(126, 38);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "Exit Game";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblShowScore
            // 
            this.lblShowScore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblShowScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblShowScore.Font = new System.Drawing.Font("Arial", 10F);
            this.lblShowScore.Location = new System.Drawing.Point(586, 54);
            this.lblShowScore.Name = "lblShowScore";
            this.lblShowScore.Size = new System.Drawing.Size(94, 29);
            this.lblShowScore.TabIndex = 14;
            this.lblShowScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLives
            // 
            this.lblLives.BackColor = System.Drawing.Color.SlateGray;
            this.lblLives.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLives.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLives.Font = new System.Drawing.Font("Arial", 10F);
            this.lblLives.ForeColor = System.Drawing.Color.White;
            this.lblLives.Location = new System.Drawing.Point(494, 100);
            this.lblLives.Name = "lblLives";
            this.lblLives.Size = new System.Drawing.Size(94, 29);
            this.lblLives.TabIndex = 15;
            this.lblLives.Text = "Lives:";
            this.lblLives.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShowLives
            // 
            this.lblShowLives.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblShowLives.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblShowLives.Font = new System.Drawing.Font("Arial", 10F);
            this.lblShowLives.Location = new System.Drawing.Point(586, 100);
            this.lblShowLives.Name = "lblShowLives";
            this.lblShowLives.Size = new System.Drawing.Size(94, 29);
            this.lblShowLives.TabIndex = 16;
            this.lblShowLives.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlayAgainMessage
            // 
            this.lblPlayAgainMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblPlayAgainMessage.Font = new System.Drawing.Font("Arial", 10F);
            this.lblPlayAgainMessage.Location = new System.Drawing.Point(462, 327);
            this.lblPlayAgainMessage.Name = "lblPlayAgainMessage";
            this.lblPlayAgainMessage.Size = new System.Drawing.Size(259, 29);
            this.lblPlayAgainMessage.TabIndex = 17;
            this.lblPlayAgainMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 436);
            this.Controls.Add(this.lblPlayAgainMessage);
            this.Controls.Add(this.lblShowLives);
            this.Controls.Add(this.lblLives);
            this.Controls.Add(this.lblShowScore);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.txtShowMessage);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Guessing Game";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtShowMessage;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblShowScore;
        private System.Windows.Forms.Label lblLives;
        private System.Windows.Forms.Label lblShowLives;
        private System.Windows.Forms.Label lblPlayAgainMessage;
    }
}

