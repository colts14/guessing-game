﻿/* =========================================================================
 *  Assignment # 6 and 7 Guessing Game
 *  
 *  Author: Group 6
 *          (Jeff, Ronald, Tharmita, Tim, Zia)
 *  
 *  July 27, 2016
 *  
 * =========================================================================
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Assignment06_Group06
{
    public partial class Form1 : Form
    {
        // Private class variables
        private static int _score;
        private static int _lives;
        private static int _randomNumber;
        private static int _guess;
        private static bool _hasWon;
        private static Random _random = new Random();
        private static Dictionary<string, object> _GGDictionary = new Dictionary<string, object>();

        public Form1()
        {
            InitializeComponent();

            // Add eventhandler to each button click event
            button1.Click += new System.EventHandler(ClickedButton);
            button2.Click += new System.EventHandler(ClickedButton);
            button3.Click += new System.EventHandler(ClickedButton);
            button4.Click += new System.EventHandler(ClickedButton);
            button5.Click += new System.EventHandler(ClickedButton);
            button6.Click += new System.EventHandler(ClickedButton);
            button7.Click += new System.EventHandler(ClickedButton);
            button8.Click += new System.EventHandler(ClickedButton);
            button9.Click += new System.EventHandler(ClickedButton);
            button10.Click += new System.EventHandler(ClickedButton);
            button11.Click += new System.EventHandler(ClickedButton);
            button12.Click += new System.EventHandler(ClickedButton);
            button13.Click += new System.EventHandler(ClickedButton);
            button14.Click += new System.EventHandler(ClickedButton);
            button15.Click += new System.EventHandler(ClickedButton);
            button16.Click += new System.EventHandler(ClickedButton);
            button17.Click += new System.EventHandler(ClickedButton);
            button18.Click += new System.EventHandler(ClickedButton);
            button19.Click += new System.EventHandler(ClickedButton);
            button20.Click += new System.EventHandler(ClickedButton);
            button21.Click += new System.EventHandler(ClickedButton);
            button22.Click += new System.EventHandler(ClickedButton);
            button23.Click += new System.EventHandler(ClickedButton);
            button24.Click += new System.EventHandler(ClickedButton);
            button25.Click += new System.EventHandler(ClickedButton);
            button26.Click += new System.EventHandler(ClickedButton);
            button27.Click += new System.EventHandler(ClickedButton);
            button28.Click += new System.EventHandler(ClickedButton);
            button29.Click += new System.EventHandler(ClickedButton);
            button30.Click += new System.EventHandler(ClickedButton);
            button31.Click += new System.EventHandler(ClickedButton);
            button32.Click += new System.EventHandler(ClickedButton);
            button33.Click += new System.EventHandler(ClickedButton);
            button34.Click += new System.EventHandler(ClickedButton);
            button35.Click += new System.EventHandler(ClickedButton);
            button36.Click += new System.EventHandler(ClickedButton);
            button37.Click += new System.EventHandler(ClickedButton);
            button38.Click += new System.EventHandler(ClickedButton);
            button39.Click += new System.EventHandler(ClickedButton);
            button40.Click += new System.EventHandler(ClickedButton);
            button41.Click += new System.EventHandler(ClickedButton);
            button42.Click += new System.EventHandler(ClickedButton);
            button43.Click += new System.EventHandler(ClickedButton);
            button44.Click += new System.EventHandler(ClickedButton);
            button45.Click += new System.EventHandler(ClickedButton);
            button46.Click += new System.EventHandler(ClickedButton);
            button47.Click += new System.EventHandler(ClickedButton);
            button48.Click += new System.EventHandler(ClickedButton);
            button49.Click += new System.EventHandler(ClickedButton);
            button50.Click += new System.EventHandler(ClickedButton);
            button51.Click += new System.EventHandler(ClickedButton);
            button52.Click += new System.EventHandler(ClickedButton);
            button53.Click += new System.EventHandler(ClickedButton);
            button54.Click += new System.EventHandler(ClickedButton);
            button55.Click += new System.EventHandler(ClickedButton);
            button56.Click += new System.EventHandler(ClickedButton);
            button57.Click += new System.EventHandler(ClickedButton);
            button58.Click += new System.EventHandler(ClickedButton);
            button59.Click += new System.EventHandler(ClickedButton);
            button60.Click += new System.EventHandler(ClickedButton);
            button61.Click += new System.EventHandler(ClickedButton);
            button62.Click += new System.EventHandler(ClickedButton);
            button63.Click += new System.EventHandler(ClickedButton);
            button64.Click += new System.EventHandler(ClickedButton);
            button65.Click += new System.EventHandler(ClickedButton);
            button66.Click += new System.EventHandler(ClickedButton);
            button67.Click += new System.EventHandler(ClickedButton);
            button68.Click += new System.EventHandler(ClickedButton);
            button69.Click += new System.EventHandler(ClickedButton);
            button70.Click += new System.EventHandler(ClickedButton);
            button71.Click += new System.EventHandler(ClickedButton);
            button72.Click += new System.EventHandler(ClickedButton);
            button73.Click += new System.EventHandler(ClickedButton);
            button74.Click += new System.EventHandler(ClickedButton);
            button75.Click += new System.EventHandler(ClickedButton);
            button76.Click += new System.EventHandler(ClickedButton);
            button77.Click += new System.EventHandler(ClickedButton);
            button78.Click += new System.EventHandler(ClickedButton);
            button79.Click += new System.EventHandler(ClickedButton);
            button80.Click += new System.EventHandler(ClickedButton);
            button81.Click += new System.EventHandler(ClickedButton);
            button82.Click += new System.EventHandler(ClickedButton);
            button83.Click += new System.EventHandler(ClickedButton);
            button84.Click += new System.EventHandler(ClickedButton);
            button85.Click += new System.EventHandler(ClickedButton);
            button86.Click += new System.EventHandler(ClickedButton);
            button87.Click += new System.EventHandler(ClickedButton);
            button88.Click += new System.EventHandler(ClickedButton);
            button89.Click += new System.EventHandler(ClickedButton);
            button90.Click += new System.EventHandler(ClickedButton);
            button91.Click += new System.EventHandler(ClickedButton);
            button92.Click += new System.EventHandler(ClickedButton);
            button93.Click += new System.EventHandler(ClickedButton);
            button94.Click += new System.EventHandler(ClickedButton);
            button95.Click += new System.EventHandler(ClickedButton);
            button96.Click += new System.EventHandler(ClickedButton);
            button97.Click += new System.EventHandler(ClickedButton);
            button98.Click += new System.EventHandler(ClickedButton);
            button99.Click += new System.EventHandler(ClickedButton);
            button100.Click += new System.EventHandler(ClickedButton);

            CreateGuessingGameDictionary();
            ResetGame();
        }
        //
        // Set up the dictionary, with key as string and value as object
        //
        private void CreateGuessingGameDictionary()
        {
            _GGDictionary.Add("startGame", "Start the Game");
            _GGDictionary.Add("failMessage", "SORRY GAME OVER");
            _GGDictionary.Add("winMessage", "CONGRATULATIONS, YOU WON THE GAME");
            _GGDictionary.Add("infMessage", "Number provided is less than the number picked by the program, ... Please try again");
            _GGDictionary.Add("supMessage", "Number provided is greater than the number picked by the program, ... Please try again");
            _GGDictionary.Add("playAgainMessage", "Would you like to play again (Y/N)?");
            _GGDictionary.Add("score", 100);
            _GGDictionary.Add("lives", 10); 
        }
        //
        // Main game logic
        //
        public void ClickedButton(object sender, EventArgs e)
        {
            int numLives = CheckNumberOfLives();

            if (numLives <= 0 || _hasWon == true)
            {
                txtShowMessage.Text = _GGDictionary.Get<string>("failMessage");
                lblPlayAgainMessage.Text = _GGDictionary.Get<string>("playAgainMessage");
                btnStart.Visible = true;
                return;
            }

            _guess = Convert.ToInt32((sender as Button).Text);

            if (_guess < _randomNumber)
            {
                ReduceScoreLives();
                (sender as Button).BackColor = Color.Pink;
                txtShowMessage.Text = _GGDictionary.Get<string>("infMessage");
            }
            else if (_guess > _randomNumber)
            {
                ReduceScoreLives();
                (sender as Button).BackColor = Color.PaleGoldenrod;
                txtShowMessage.Text = _GGDictionary.Get<string>("supMessage");
            }
            else
            {
                (sender as Button).BackColor = Color.LightGreen;
                txtShowMessage.Text = _GGDictionary.Get<string>("winMessage");
                lblPlayAgainMessage.Text = _GGDictionary.Get<string>("playAgainMessage");
                btnStart.Visible = true;
                _hasWon = true;
                return;
            }
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Visible = false;
            ResetGame();
        }
        private int GenerateRandomNumber()
        {
            return _random.Next(1, 101);
        }
        private int CheckNumberOfLives()
        {
            return _GGDictionary.Get<int>("lives");
        }
        //
        // Score and lives values and update the dictionary with the new values
        //
        private void ReduceScoreLives()
        {
            _score -= 10;
            _lives--;
            _GGDictionary["score"] = _score;
            _GGDictionary["lives"] = _lives;
            lblShowScore.Text = _GGDictionary.Get<int>("score").ToString();
            lblShowLives.Text = _GGDictionary.Get<int>("lives").ToString();
        }
        private void ResetGame()
        {
            _score = 100;
            _lives = 10;
            _GGDictionary["score"] = _score;
            _GGDictionary["lives"] = _lives;
            _randomNumber = GenerateRandomNumber();
            txtShowMessage.Text = "";
            lblPlayAgainMessage.Text = "";
            lblShowScore.Text = _GGDictionary.Get<int>("score").ToString();
            lblShowLives.Text = _GGDictionary.Get<int>("lives").ToString();
            btnStart.Text = _GGDictionary.Get<string>("startGame");
            _hasWon = false;

            // Iterate through collection of controls in groupbox
            // and reset button's backcolor to white
            foreach (var button in groupBox1.Controls)
            {
                Button myButton = null;

                try
                {
                    myButton = (Button)button;
                }
                catch (InvalidCastException e)
                {
                    // Do nothing
                }
                if (myButton != null)
                {
                    int num;
                    bool result = int.TryParse(myButton.Text, out num);
                    if (num >= 1 && num <= 100)
                    {
                        myButton.BackColor = Color.White;
                    }
                }
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
